# php Db browser For sqlite

#### 介绍
php Db browser For sqlite （php sqlite browser）php版sqlite数据库浏览器

#### 功能

列出网站目录下的sqlite数据并浏览：
1. 遍历文件夹下所有.db,.sqlite文件供选并记忆；
2. 然后该数据文件下所有表供选并记忆；
3. 列出该表下所有字段及内容；
由于不保证每个数据都有唯一不重复字段，暂无表的增改删计划
未设密码，推荐文件夹下更名为任意文件名使用以保障数据安全

#### 问题反馈
问题反馈：15058593138 (同微信号)
或发邮件：admin@ewuyi.net

#### 另外可能后续开发
php access;php mysql;php csv;asp access;asp excel
等版本,敬请关注
